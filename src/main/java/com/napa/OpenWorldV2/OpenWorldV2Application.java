package com.napa.OpenWorldV2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenWorldV2Application {

	public static void main(String[] args) {
		SpringApplication.run(OpenWorldV2Application.class, args);
	}

}
